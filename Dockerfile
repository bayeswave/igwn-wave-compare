
# syntax=docker/dockerfile:1
FROM python:slim

LABEL name="Igwn Wave Compare: Base Distribution" \
      maintainer="Jack Sullivan <johnmichael.sullivan@ligo.org>" \
      date="20230716" \
      support="Experimental Platform"

# Create both users (admin and analysis); last one (root) is so we can set things up properly
# USER root 

# SHELL ["bash","-c"]

WORKDIR /app
RUN mkdir -p /cvmfs /hdfs /gpfs /ceph /hadoop /etc/condor

COPY . /src

RUN apt-get update && apt-get -y upgrade \
&& apt-get install -y --no-install-recommends \
      git \
      wget \
      g++ \
      gcc \
      ca-certificates \
      && rm -rf /var/lib/apt/lists/*

RUN wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh \
    && mkdir /root/.conda \
    && bash Miniconda3-latest-Linux-x86_64.sh -b \
    && rm -f Miniconda3-latest-Linux-x86_64.sh 

ENV PATH="/root/miniconda3/bin:${PATH}"
ARG PATH="/root/miniconda3/bin:${PATH}"

# RUN . /root/.bashrc 
# RUN /bin/bash . /root/.bashrc 

RUN conda update conda \
      && conda create -n iwc \
      && conda activate iwc 

RUN conda install python=3.10.12 pip

RUN pip install -r /src/requirements_container.txt

# install igwn_wave_compare
RUN cd /src/ && pip install .




# # WORKDIR /app

# # RUN apt-get update -y && apt-get upgrade -y && && apt-get -y install python3.10 && apt-get -y install python3-scipy && apt-get -y install python3-pip
# # && apt-get -y install python3.10


# # Setup directories for binding (direct reading on file host[execute nodes])
# RUN mkdir -p /cvmfs /hdfs /gpfs /ceph /hadoop /etc/condor

# # copy contents igwn-wave-compare to /src in container
# COPY . /src

# # COPY requirements_container.txt /src/requirements_container.txt

# COPY container/entrypoint.sh /usr/local/bin/
# RUN ln -s usr/local/bin/entrypoint.sh / # backwards compat
# ENTRYPOINT ["entrypoint.sh"] 

# # Python/pip/setuptools configured using base image
# RUN python3 -m pip install -r /src/requirements_container.txt

# # install igwn_wave_compare
# RUN python3 -m pip install /src/

# # ## create analysis user and make them default at entry
# # RUN useradd -ms /bin/bash analy.user
# # USER analy.user


