import argparse
import os
import sys
import pickle

import numpy as np
# import scipy.signal

# plt.rcParams["font.family"] = "sans-serif"
# plt.rcParams["font.sans-serif"] = "Arial"


import igwn_wave_compare as iwc
from gwpy.timeseries import TimeSeries as TS

import pycbc.types

# import pycbc.frame
from pycbc.filter import resample_to_delta_t
# from pycbc.inject import InjectionSet

### MARK DEV: dev import:
import igwn_wave_compare.filt as filt

import matplotlib
from matplotlib import pyplot as plt
matplotlib.use("Agg")

### dev functions for testing outputs
def load(path):
    f = open(path, 'rb' )
    return pickle.load(f)

def save(df, path):
    f = open(path, 'wb' )
    pickle.dump(df,f)
    return

def parser():

    parser = argparse.ArgumentParser(description=__doc__,
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--workdir", type=str, required=True, help='Path to residual_frame directory to dump plots into. Assuming saved analysis data into npz file in residual_frame.')

    return parser.parse_args()

# plt.rcParams.update({
# "text.usetex": True,
# "font.family": "sans-serif",
# "font.sans-serif": ["Helvetica"]})

### call opts; different than args (which we load)
opts = parser()


### if pass relative path; absolutely path
if opts.workdir == './residual_frame':
    opts.workdir = os.path.join(os.getcwd(), 'residual_frame')   
elif opts.workdir == '.':
    opts.workdir = os.getcwd()
    
if 'residual_frame' not in opts.workdir:
    raise ValueError('Make sure that a path to an onsource_residuals/residual_frame directory is passed.')
    
## now check to make sure path exists
if not os.path.exists(opts.workdir):
    raise ValueError('Please enter a valid path to a onsource_residuals/residual_frame directory.')


### hack lifted from stack exchange to allow for pickled loading
# np_load_old = np.load

# modify the default parameters of np.load
# np.load = lambda *a,**k: np_load_old(*a, allow_pickle=True, **k)
loaded_data = load(os.path.join(opts.workdir, 'res-analy-dat.p'))
globals().update(loaded_data)

args.output_path = opts.workdir


print('Residual Data Loaded\n--------------------------')

print("Making diagnostic plots")

### load all data from residuals job into global variables
# for key, value in loaded_data.items():
#     globals()[key] = value



# ### overwrite existing output path

#
# Filter (bandpass and notches)
#
print("Filtering")

filterBas = filt.get_filt_coefs(data[0].sample_rate, 30, 300, False, False)
filtered_data = [
    pycbc.types.TimeSeries((filt.filter_data(d.data, filterBas)), epoch=epoch_padded, delta_t=delta_t) for d in
    data]
filtered_residuals = [
    pycbc.types.TimeSeries((filt.filter_data(d.data, filterBas)), epoch=epoch_padded, delta_t=delta_t) for d in
    residuals]
filtered_waveforms = [
    pycbc.types.TimeSeries((filt.filter_data(d.data, filterBas)), epoch=epoch_padded, delta_t=delta_t) for
    d in waveforms]

# T-domain
times = waveforms[0].sample_times - args.trigtime


f, ax = plt.subplots(nrows=len(ifos), figsize=(10,6*len(ifos)))

for i in range(len(ifos)):
    ax[i].plot(times, filtered_data[i], label='data')
    ax[i].plot(times, filtered_waveforms[i], label='model')
    ax[i].plot(times, filtered_residuals[i], label='residuals')
    # ax[i].set_ylim(-4,4)
    ax[i].set_xlim(-0.2, 0.2)
    ax[i].set_title(ifos[i])
    ax[i].set_xlabel('Time from trigger [s]')
    if ifos[i] == 'V1':
        ax[i].set_ylim(-5e-21, 5e-21)
    else:
        ax[i].set_ylim(-1e-21, 1e-21)
ax[0].legend(loc='upper left')

f.tight_layout(rect=[0, 0.03, 1, 0.95]) ### O4: Missing font libraries
plt.savefig(os.path.join(args.output_path, 'residuals_diagnostics_TD.png'))

# ASD colored data
plt.close('all')

f, ax = plt.subplots(nrows=len(ifos), figsize=(10, 6*len(ifos)))
fftnorm = np.sqrt(2. / args.srate)
for i in range(len(ifos)):
    dataF = filtered_data[i].to_frequencyseries() / fftnorm
    waveformF = filtered_waveforms[i].to_frequencyseries() / fftnorm
    residualsF = filtered_residuals[i].to_frequencyseries() / fftnorm

    ax[i].loglog(dataF.sample_frequencies, abs(dataF),
                    label='Data')
    ax[i].loglog(waveformF.sample_frequencies, abs(waveformF),
                    label='Model')
    ax[i].loglog(residualsF.sample_frequencies, abs(residualsF),
                    label='Residuals')

    ax[i].set_xlabel(f'Frequency [Hz]')
    ax[i].set_title(ifos[i])
ax[0].legend(loc='lower left')
# f.tight_layout()
f.tight_layout(rect=[0, 0.03, 1, 0.95])

plt.savefig(os.path.join(args.output_path, 'residuals_diagnostics_FD.png'))
plt.close('all')

if args.psd_files is not None:

    # Adjust times so that zero is on an integer second
    trigtime, delta = divmod(args.trigtime, 1)
    local_times = waveforms_wt[0].sample_times - args.trigtime

    f, ax = plt.subplots(figsize=(4*len(ifos)+2, 4), nrows=1, ncols=len(ifos))

    for i in range(len(ifos)):
        ax[i].plot(local_times, waveforms_wt[i])
        ax[i].set_xlim(-0.075, 0.075)
        ax[i].set_xlabel('Time from %d + %.4f [s]' % (int(trigtime), delta))
        ax[i].set_ylabel('%s Whitened Strain' % ifos[i])
        ax[i].minorticks_on()

    plt.savefig(os.path.join(args.output_path, 'whitened_waveform.png'))
    if args.make_omega_scans:
        if not args.frame_cache:
            raise ArgumentError('pass --frame-cache in addition to --omega-scans ')
        print("Making Omega scans")
        
        ### MARK DEV: TODO: custom flag for q-resolution based on signal? e
        f_range = (9, 0.8 * args.srate / 2)
        q_range = (10, 12)
        outseg = (np.floor(args.trigtime - 2), np.floor(args.trigtime + 2))
        qscans_data = {}
        print("Reading in Data")
        for i, ifo in enumerate(ifos):
            print(ifo)

            temp = TS.read(inFrames[i], args.input_channel[i], epoch_padded, 
                        epoch_padded + duration_padded, format='gwf').to_pycbc()
            # OLD:   
            # temp = pycbc.frame.read_frame(args.frame_cache[i], args.input_channel[i]
            #                               , args.trigtime - 2, args.trigtime + 2)
            # temp = pycbc.frame.read_frame(args.frame_cache[i], args.input_channel[i]
            #                               , np.floor(args.trigtime - 2), np.floor(args.trigtime + 2))
            

                
            # temp = TS.fetch(f"{0}".format(args.input_channel[i]),
            # epoch_padded, epoch_padded + duration_padded, verbose=True)
            # data = [TS.fetch("{0}:{1}".format(ifo, args.input_channel[i]),
            # epoch_padded, epoch_padded + duration_padded, verbose=True) for i, ifo in enumerate(ifos)]

            temp = resample_to_delta_t(temp, 1. / args.srate)
            times = temp.sample_times
            delta_t = temp.delta_t
            temp = iwc.strain.whiten_strain(temp, psds[i])
            temp = TS(data=np.array(temp.tseries), dt=delta_t, t0=times[0]) ### MARK DEV: don't know that we need this 
            qscans_data[ifo] = temp.q_transform(frange=f_range, qrange=q_range,
                                                outseg=outseg, whiten=False)

        qscans_res = {}
        print("Reading in residuals")
        for i, ifo in enumerate(ifos):
            print(ifo)
            # temp = pycbc.frame.read_frame(frame_paths[i], args.output_channel[i]
            #                               , np.floor(args.trigtime - 2), np.floor(args.trigtime + 2))
            # temp = resample_to_delta_t(temp, 1. / args.srate)
            
            temp = residuals[i] ### sub in residuals here from above; convert to time series 
            times = temp.sample_times
            delta_t = temp.delta_t
            temp = iwc.strain.whiten_strain(temp, psds[i])
            temp = TS(data=np.array(temp.tseries), dt=delta_t, t0=times[0])
            qscans_res[ifo] = temp.q_transform(frange=f_range, qrange=q_range,
                                                outseg=outseg, whiten=False)

        qtimes_data = qscans_data[ifos[0]].xindex.value
        qfreqs_data = qscans_data[ifos[0]].yindex.value

        qtimes_res = qscans_res[ifos[0]].xindex.value
        qfreqs_res = qscans_res[ifos[0]].yindex.value
        plt.close()

        fbig, axbig = plt.subplots(figsize=(len(ifos)*8+2, 8),
                                    ncols=len(ifos), nrows=2,
                                    squeeze=False)

        ifo_names = {'H1': 'Hanford', 'L1': 'Livingston', 'V1': 'Virgo'}

        nanosecond, origin = np.modf(args.trigtime)
        for row in range(2):
            print("on row ", row)
            for col, ifo in enumerate(sorted(qscans_data.keys())):
                if row == 0:
                    print('Plotting data qscans')
                    pcol = axbig[row][col].pcolormesh(qtimes_data - origin, qfreqs_data,
                                                        4.0 / np.pi * qscans_data[ifo].T,
                                                        vmin=0, vmax=25.5,
                                                        cmap='viridis', shading='gouraud',
                                                        rasterized=True)

                    if ifo == 'H1':
                        axbig[row][col].set_ylabel(f'Data Frequency [Hz]')

                    axbig[row][col].set_ylim(10, 0.75 * args.srate / 2)
                    axbig[row][col].minorticks_on()
                    axbig[row][col].set_yscale('log')
                    axbig[row][col].set_title('%s' % ifo_names[ifo])

                if row == 1:
                    print('Plotting res qscans')
                    pcol = axbig[row][col].pcolormesh(qtimes_res - origin, qfreqs_res,
                                                        4.0 / np.pi * qscans_res[ifo].T,
                                                        vmin=0, vmax=25.5,
                                                        cmap='viridis', shading='gouraud',
                                                        rasterized=True)
                    if ifo == 'H1':
                        axbig[row][col].set_ylabel(f'Residual Frequency [Hz]')

                    axbig[row][col].set_ylim(10, 0.75 * args.srate / 2)
                    axbig[row][col].minorticks_on()
                    axbig[row][col].set_yscale('log')
                    axbig[row][col].set_xlabel('Time [s]')

                axbig[row][col].set_xlim(-0.4 + nanosecond + 0.02, 0.025 + nanosecond + 0.20)

        plt.subplots_adjust(left=0.075, right=0.925, bottom=0.09, top=0.95,
                            wspace=0.1, hspace=0.1)

        cbaxes = fbig.add_axes([.93, 0.2, 0.015, 0.7])
        cbar = fbig.colorbar(pcol,
                                orientation='vertical',
                                cax=cbaxes)
        for axis in ['top', 'bottom', 'left', 'right']:
            cbaxes.spines[axis].set_linewidth(0.25)

        cbar.set_label(f'Normalized Energy')
        cbar.ax.xaxis.set_label_position('top')

        print('Saving')
        # fbig.tight_layout(rect=[0, 0.03, 1, 0.95])
        # plt.savefig(os.path.join(args.output_path, 'white_data_res.pdf'))
        plt.savefig(os.path.join(args.output_path, 'white_data_res.png'))
