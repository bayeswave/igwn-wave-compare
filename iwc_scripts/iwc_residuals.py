#!/cvmfs/software.igwn.org/conda/envs/igwn/bin/python
# # # -*- coding: utf-8 -*-
# Copyright (C) 2017-2018 James Clark <james.clark@ligo.org>
#               2017-2020 Sudarshan Ghonge <sudarshan.ghonge@ligo.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import argparse
import os
import sys
import types
import pickle

import numpy as np
import scipy.signal
from copy import deepcopy

import subprocess

from glue.ligolw import ligolw
from glue.ligolw import lsctables

import igwn_wave_compare as iwc
import igwn_wave_compare.filt as filt

from gwpy.timeseries import TimeSeries

import pycbc.frame
import pycbc.types
from pycbc.filter import resample_to_delta_t
from pycbc.inject import InjectionSet

from pprint import pprint

import matplotlib
matplotlib.use("Agg")
from matplotlib import pyplot as plt

import pandas as pd 

### EXPLICITLY TURNING OFF
# plt.rc('text', usetex=False)

plt.rcParams["font.family"] = "sans-serif"
plt.rcParams["font.sans-serif"] = "Arial"

fig_width_pt = 4 * 246.0  # Get this from LaTeX using \showthe\columnwidth
inches_per_pt = 2.0 / 72.27  # Convert pt to inches
golden_mean = (np.sqrt(5) - 1.0) / 2.0  # Aesthetic ratio
fig_width = fig_width_pt * inches_per_pt  # width in inches
fig_height = fig_width * golden_mean  # height in inches
fig_size = [fig_width, fig_height]
fontsize = 18
params = {
    'text.usetex': False,
    'axes.labelsize': fontsize,
    'font.size': fontsize,
    'legend.fontsize': fontsize,
    'xtick.color': 'k',
    'xtick.labelsize': fontsize,
    'ytick.color': 'k',
    'ytick.labelsize': fontsize,
    # 'text.usetex': True,
    'text.color': 'k',
    'figure.figsize': fig_size
}

plt.rcParams.update(params)

def load(path):
    f = open(path, 'rb' )
    return pickle.load(f)

def save(obj, path):
    with open(path, 'wb') as f:
        pickle.dump(obj,f, protocol=pickle.HIGHEST_PROTOCOL)
    return

lsctables.use_in(ligolw.LIGOLWContentHandler)

def parser():

    parser = argparse.ArgumentParser(description=__doc__,
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--li-samples", type=str, required=True)
    parser.add_argument("--pesumm-label", type=str, default=None)
    parser.add_argument("--frame-cache", type=str, nargs='+', default=None)
    parser.add_argument("--use-frame-files", default=False, action="store_true")
    parser.add_argument("--srate", type=float, default=16384)
    parser.add_argument("--trigtime", type=float, default=1167559936.6)
    parser.add_argument("--epoch", type=float, default=None)
    parser.add_argument("--duration", type=float, default=8.0)
    parser.add_argument("--output-frame-type", type=str, nargs='+', required=True)
    parser.add_argument("--input-channel", type=str, nargs='+', required=True)
    parser.add_argument("--ifos", type=str, nargs='+', choices=["H1", "L1", "V1"], required=True)
    parser.add_argument("--output-channel", type=str, nargs='+', required=True)
    parser.add_argument("--make-plots", default=False, action="store_true")
    parser.add_argument("--output-path", type=str, default="./")
    parser.add_argument("--waveform-fmin", default=10, type=float)
    parser.add_argument("--use-maxL", default=False, action="store_true")
    parser.add_argument("--numrel-data", default=None)
    parser.add_argument("--psd-files", type=str, nargs='+', default=None,
                        help='PSD files if whitened waveform debugging plots are needeed')
    parser.add_argument("--asd-files", type=str, nargs='+', default=None,
                        help='ASD files if whitened waveform debugging plots are needeed for simulated onsource runs (not currently supported)')
    parser.add_argument("--make-omega-scans", default=False, action="store_true",
                        help="Make Omega scan plots of data and residual")
    parser.add_argument("--calibrate", default=False, action="store_true",
                        help="Whether to use calibration factors.Use this only when dealing with real data")
    parser.add_argument("--choose-fd", default=False, action="store_true",
                        help="Whether to use frequency domain wavform.")
    parser.add_argument("--flow", default=20.0,
                        help="Flow of the waveform that'll get generated. Defaults to what's in the PE file",
                        type=float)
    parser.add_argument("--approx", default=None, help="Explicitly write out the string name of the approximant")
    parser.add_argument("--fref", default=None, type=float)
    parser.add_argument("--amp-order", default=None, type=int)
    parser.add_argument("--phase-order", default=None, type=int)

    parser.add_argument("--dev-plot-bypass", default=False, action="store_true", help='Bypass plotting to do locally (manually). Due to issues with Tex files in condor env (matplotlib).')
    parser.add_argument("--singularity-dir-make", default=False, action="store_true",
                        help="Make output directories in container for binding to output.")
    parser.add_argument("--MDC-cache", type=str, default=None,
                        help="Path of MDC cache frame files.")
    parser.add_argument("--sim-onsource-residuals", default=False, action="store_true", help='Create residuals for simulated onsource residuals run.')
    parser.add_argument("--cust-bw-seglen", default=None, type=float, help='Custom window to use for onsource residuals run. \
                            Pads residuals frames with zeros to enable BW onsource residuals runs to have longer analysis windows.')
    parser.add_argument("--fhigh", default=1024.0, type=float, help='PE fhigh.')

    args = parser.parse_args()

    return args

# def main(args):
#######################
        
   
args = parser()

ifos = args.ifos
if len(ifos) != len(args.input_channel):
    raise ValueError('Need as many input channels as ifos')

if len(ifos) != len(args.output_channel):
    raise ValueError('Need as man output channels as ifos')

if len(ifos) != len(args.output_frame_type):
    raise ValueError('Need as many output frame types as ifos')

if args.asd_files is not None and args.psd_files is not None:
    print('Error: --psd-files and --asd-files both passed. Choose one or the other and try again.')
    sys.exit(1)
elif not args.sim_onsource_residuals and args.psd_files is None:
    print('Error: --psd-files not passed. Ensure PSD files are passed and try again')
    sys.exit(1)
elif args.sim_onsource_residuals and args.asd_files is None:
    print('Error: --sim-onsource-residuals passed without --asd-files. Ensure ASD files are passed and try again')
    sys.exit(1)

if args.flow is None:
    print('Error: flow not passed. Check args and try again.')
    sys.exit(1)
if args.fhigh is None:
    print('Error: fhigh not passed. Check args and try again.')
    sys.exit(1)
     

if args.epoch is None:
    if not args.use_frame_files:
        epoch_padded = np.floor(args.trigtime - args.duration)
        duration_padded = 2. * args.duration
    else:
        epoch_padded = np.floor(args.trigtime - args.duration / 2.0)
        duration_padded = args.duration
        
else:
    if not args.use_frame_files:
        epoch_padded = np.floor(args.epoch)
        duration_padded = 2. * args.duration
    else:
        epoch_padded = np.floor(args.epoch)
        duration_padded = args.duration
if args.phase_order is None: 
    phase_ord = -1 ### O4: by default, use all implemented post-Newtonian orders
else: 
    phase_ord = args.phase_order

li_samples = np.genfromtxt(args.li_samples, names=True)

# If args.use_maxL, then use the CBC max Likelihood (ML) waveform to make the residuals. Else, use the CBC max aPosteriori (MAP) waveform
if args.use_maxL:
    sampleType='singleLogL' ### ML Waveform
else:
    sampleType='singleLogPost'  ### MAP Waveform


if args.calibrate:
    calibration_frequencies = iwc.posterior.extract_calibration_frequencies(li_samples, ifos) 
    if calibration_frequencies is None:
        print("Cannot find calibration frequencies. Skipping calibration")
else:
    calibration_frequencies = None

### Saving of WF making data for later testing, if desired
pklWfDat = {
    'samples': li_samples,
    'duration': args.duration, 
    'epoch': args.epoch,
    'sample_rate':args.srate,
    'ifos':ifos,
    'choose_fd':args.choose_fd,
    'flow':args.flow,
    'calibration_frequencies': calibration_frequencies,
    'fref':args.fref,
    'amp_order':args.amp_order,
    'phase_order':phase_ord, 
    'sampleType':sampleType,
    'approx': args.approx
}

wfArgsFile = os.path.join(args.output_path, 'make_wf_data.pickle')
with open(wfArgsFile, 'wb') as handle:
    pickle.dump(pklWfDat, handle, protocol=pickle.HIGHEST_PROTOCOL)

########################### Real Data (Frames): Onsource Residuals ###########################
if not args.sim_onsource_residuals:
    print("Generating waveform from posterior samples")

    if args.approx is None:
        print('Error: --approx not passed. Check call of iwc_residuals.py and try again.')
        sys.exit(1)
    else:
        waveformsF = iwc.waveform.generate_strains_from_samples(samples=li_samples,
                                                                duration=duration_padded, epoch=epoch_padded,
                                                                sample_rate=args.srate, ifos=ifos,
                                                                calibration_frequencies=calibration_frequencies,
                                                                choose_fd=args.choose_fd,
                                                                flow=args.flow, approx=args.approx,
                                                                fref=args.fref, amp_order=args.amp_order, phase_order=phase_ord, 
                                                                sampleType=sampleType)

    waveformsF = [h[0] for h in waveformsF]
    waveforms = [h.tseries for h in waveformsF]

    # if args.psd_files is not None, create PSD interpolants
    if len(args.psd_files) != len(args.input_channel):
        raise ValueError('Number of PSD files need to be\
                        same as number of channels')
    else:
        ### generate psd interepolants; whiten waveforms
        psds = [iwc.psd.interp_from_txt(x, asd_file=False) for x in args.psd_files]
        waveforms_wf = [iwc.strain.whiten_strain(h, psd) for h, psd in zip(waveformsF, psds)]
        waveforms_wt = [h.tseries for h in waveforms_wf]
    
    
    
    ### compute optimal network snr (MAP PE Waveform) for fitting factor analysis
    waveformsF_MAP = iwc.waveform.generate_strains_from_samples(samples=li_samples,
                                                                duration=duration_padded, epoch=epoch_padded,
                                                                sample_rate=args.srate, ifos=ifos,
                                                                calibration_frequencies=calibration_frequencies,
                                                                choose_fd=args.choose_fd,
                                                                flow=args.flow, approx=args.approx,
                                                                fref=args.fref, amp_order=args.amp_order, phase_order=phase_ord, 
                                                                sampleType='singleLogPost')
    waveformsF_MAP = [h[0] for h in waveformsF_MAP]
    waveforms_MAP = [h.tseries for h in waveformsF_MAP]
    ### create PSD's on correct sampling frequency/delta_f for waveformsF_MAP
    psds_interp = iwc.psd.interpolate_psds_fromWF(waveformsF_MAP, psd_interpolants=psds)
    snr_gr = iwc.waveform.optimal_network_snr(waveformsF_MAP, psds=psds_interp)
    snr_gr = '%.10f' % snr_gr
    with open(os.path.join(args.output_path, 'SNR_GR.txt'), 'w+') as f:
        f.write(snr_gr)
    
    if not args.frame_cache and args.use_frame_files:
        raise TypeError('Currently required to use: --frame-cache to specifiy cache files and --use-frame-files.')

    if args.frame_cache is not None:
        if len(args.frame_cache) != len(args.input_channel):
            raise ValueError("Need as many frame caches as ifos")

    # -------- Load Data -------- #
    print("Reading Data")

    if args.frame_cache is None: #and args.injection_file is None: ### deprecated (for now)
        ### No frame cache, no sim-inspiral table; fetch data (to subtract from) using gwpy.

        print("Fetching Strain Data")
        ### get detector data per ifo
        data = [TimeSeries.fetch("{0}:{1}".format(ifo, args.input_channel[i]),
                        epoch_padded, epoch_padded + duration_padded, verbose=True) for i, ifo in enumerate(ifos)]
        ### convert each to pycbc timeseries for further manipulation
        data = [pycbc.types.TimeSeries(d.value, epoch=epoch_padded, delta_t=d.dx.value)
                for d in data]
                    
    ### if we have a cache file
    elif args.frame_cache is not None: #and args.injection_file is None: ### deprecated (for now)
       
        print("Reading directly from frame files")

        ### get gwf from cache files
        inFrames = []
        for ind, f in enumerate(args.frame_cache):
            with open(f) as file:
                cont = file.read().split(' ')
                inFrames.append(cont[4])
                
        ### Reading frames: intermediary layer
        for ind in range(len(inFrames)):
            if 'localhost' in inFrames[ind]:
                ### found using datafind, localhost URL accessible
                continue
            else:
                ### assume frames transferred in condor, take away full path in submit filesystem 
                inFrames[ind] = os.path.basename(inFrames[ind]) 
        
        ### Updated for O4: use gwpy for I/O due to PyCBC reading bugs; can get equivalent functionality later - seems an ok swap for just file I/O
        data = [TimeSeries.read(inFrames[i], args.input_channel[i], epoch_padded, 
                                epoch_padded + duration_padded, format='gwf').to_pycbc() for i in range(len(ifos))]

    else:
        ### Other combinations not currently supported
        print("This combination of data args not currently supported", file=sys.stderr)    

    if args.MDC_cache is not None:
        print('Error: use of --MDC-cache not currently supported.')
        sys.exit(1)


    delta_t = waveforms[0].delta_t
    for x in range(len(ifos)):
        ### assuming frame data fetched at minimum desired srate
        if len(data[x]) != len(waveforms[x]):
            data[x] = data[x].resample(delta_t)
        
    ### calculate residuals
    print("Finding Residuals")
    ### make residuals time series
    residuals = [np.array(data[i]) - np.array(waveforms[i]) for i in range(len(ifos))]
    residuals = [pycbc.types.TimeSeries(d, epoch=epoch_padded, delta_t=delta_t) for d in residuals]
    

########################### TGR MDC (Simulated Onsource Residuals) ###########################
 
else:
    if args.MDC_cache is not None:
        
        if not os.access(args.MDC_cache, os.R_OK):
            print(f"Error: The mdc cache file {args.MDC_cache} is not readable.")
            sys.exit(1)
        else:
            print(f'MDC cache file {args.MDC_cache} readable.')
        
        
        with open(args.MDC_cache, 'r') as cf: 
            lines = cf.readlines()
            
        mdcFrame = []
        for l in lines: 
            items = l.split(' ')
            mdcFrame.append(items[4]) ### MDC channel names from MDC.cache
        if len(mdcFrame) != len(ifos):
            print('Error: not as many frames read as IFOs for this analysis in MDC cache file. Check your opts and try again.')
            sys.exit(1)
            
        ### read in MDC waveforms; data is mdc frame(s) for noiseless MDC's
        data = [TimeSeries.read(mdcFrame[i], args.input_channel[i], format='gwf').to_pycbc() for i in range(len(ifos))]
                
        ### Overwrite padded duration and epoch with bounds of MDC.cache
        duration_padded = np.ceil(data[0].sample_times[-1] - data[0].sample_times[0])
        
        epoch_padded = np.floor(data[0].sample_times[0])
        
        print("Generating waveform from posterior samples")
        if args.approx is None:
            print('Error: --approx not passed. Check call of iwc_residuals.py and try again.')
            sys.exit(1)
        else:
            waveformsF = iwc.waveform.generate_strains_from_samples(samples=li_samples,
                                                                    duration=duration_padded, epoch=epoch_padded,
                                                                    sample_rate=args.srate, ifos=ifos,
                                                                    calibration_frequencies=calibration_frequencies,
                                                                    choose_fd=args.choose_fd,
                                                                    flow=args.flow, approx=args.approx,
                                                                    fref=args.fref, amp_order=args.amp_order, phase_order=phase_ord, 
                                                                    sampleType=sampleType)

        waveformsF = [h[0] for h in waveformsF]
        waveforms = [h.tseries for h in waveformsF]


        if len(args.asd_files) != len(args.input_channel):
            raise ValueError('Number of PSD files need to be\
                            same as number of channels')
        else:
            ### generate psd interepolants; whiten waveforms
            psds = [iwc.psd.interp_from_txt(x, asd_file=True) for x in args.asd_files]
            waveforms_wf = [iwc.strain.whiten_strain(h, psd) for h, psd in zip(waveformsF, psds)]
            waveforms_wt = [h.tseries for h in waveforms_wf]
      
        ### compute optimal network snr (MAP PE Waveform) for fitting factor analysis
        waveformsF_MAP = iwc.waveform.generate_strains_from_samples(samples=li_samples,
                                                                    duration=duration_padded, epoch=epoch_padded,
                                                                    sample_rate=args.srate, ifos=ifos,
                                                                    calibration_frequencies=calibration_frequencies,
                                                                    choose_fd=args.choose_fd,
                                                                    flow=args.flow, approx=args.approx,
                                                                    fref=args.fref, amp_order=args.amp_order, phase_order=phase_ord, 
                                                                    sampleType='singleLogPost')
        waveformsF_MAP = [h[0] for h in waveformsF_MAP]
        waveforms_MAP = [h.tseries for h in waveformsF_MAP]
        ### create PSD's on correct sampling frequency/delta_f for waveformsF_MAP
        psds_interp = iwc.psd.interpolate_psds_fromWF(waveformsF_MAP, psd_interpolants=psds)
        snr_gr = iwc.waveform.optimal_network_snr(waveformsF_MAP, psds=psds_interp)
        snr_gr = '%.10f' % snr_gr
        with open(os.path.join(args.output_path, 'SNR_GR.txt'), 'w+') as f:
            f.write(snr_gr)
    
        ### downsampling only for each; wf calculated with analysis srate
        ### resample to WF if srate is lower there than for MDC
        delta_t = waveforms[0].delta_t
        for x in range(len(ifos)):
            if len(data[x]) > len(waveforms[x]):
                data[x] = data[x].resample(delta_t) 
            elif len(data[x]) < len(waveforms[x]):
                print('Error: Length of MDC waveform is less than the length of the max log likelihood waveform created at the PE sampling rate.')
                print('Check your MDC frame file, and try again.')
                sys.exit(1)
                
        ### make residuals time series
        print("Finding Residuals")
        residuals = [np.array(data[i]) - np.array(waveforms[i]) for i in range(len(ifos))]
        residuals = [pycbc.types.TimeSeries(r, epoch=epoch_padded, delta_t=delta_t) for r in residuals]
        
    else:
        print('Error: --MDC-cache not passed with --sim-onsource-residuals. Check args and try again.')
        sys.exit(1)
        
    ### custom seglength for MDC runs
    if args.cust_bw_seglen:
        beg_needed = args.trigtime - args.cust_bw_seglen / 2 
        end_needed = args.trigtime + args.cust_bw_seglen / 2
        if beg_needed < data[0].sample_times[0]:
            ### assuming padding with integer number of samples for extended BW seglen; data already created at same length
            zeros_num_before = (data[0].sample_times[0] - beg_needed) * args.srate
            for x in range(len(ifos)):
                data[x].prepend_zeros(zeros_num_before)
                waveforms[x].prepend_zeros(zeros_num_before)
                residuals[x].prepend_zeros(zeros_num_before)
            epoch_padded = beg_needed
        if data[0].sample_times[-1] < end_needed:
            zeros_num_after = (end_needed - data[0].sample_times[0]) * args.srate
            for x in range(len(ifos)):
                data[x].append_zeros(zeros_num_after)
                waveforms[x].append_zeros(zeros_num_after)
                residuals[x].append_zeros(zeros_num_after)
            duration_padded = end_needed - epoch_padded
    if len(residuals[0]) == len(waveforms[0]) and len(waveforms[0]) == len(data[0]):
        print('Lengths good before slicing')
        print(len(data[0]))
    else:
        print('Error: new lengths of time series\' not the same before slicing')


### save data for more diagnostic plotting later
datDict = {
    'residuals': residuals,
    'data': data,
    'waveforms': waveforms
}

datFile = os.path.join(args.output_path, 'analy_dat.pickle')
with open(datFile, 'wb') as handle:
    pickle.dump(datDict, handle, protocol=pickle.HIGHEST_PROTOCOL)

########################### DATA WRITING ###########################

frame_paths = []

### REAL ONSOURCE RESIDUALS DATA WRITING
if not os.path.isdir(args.output_path):
    os.makedirs(args.output_path)

if not args.sim_onsource_residuals: 

    for i, ifo in enumerate(ifos):
        
        residuals[i] = residuals[i].time_slice(epoch_padded, epoch_padded + duration_padded)        
        if not args.sim_onsource_residuals: 
            ### write individual simulated frames
            print("writing frames")
            frame_name = "{obs}-{frame_type}-{epoch}-{duration}.gwf".format(
                obs=ifo.replace('1', ''), ifo=ifo, frame_type=args.output_frame_type[i],
                epoch=int(epoch_padded), duration=int(duration_padded))
            
            print(f'WRITING FRAME: {frame_name}')
            
                
            frame_path = os.path.join(args.output_path, frame_name)
            frame_paths.append(frame_path)
            pycbc.frame.frame.write_frame(frame_path, args.output_channel[i], residuals[i])
            os.chmod(frame_path, 0o755)

            ### Make caches for real onsource residuals runs
            a = os.popen(f'ls {args.output_path}/*%s*.gwf | lal_path2cache'%ifo).read().strip("\n").split(' ')
            a[4] = os.path.basename(a[4].split('localhost')[-1])
            a[4] = os.path.join(f'{args.output_path}', a[4])
            cacheName = os.path.join('datafind', '%s_RES.cache'%ifo)
            with open(cacheName, 'w+') as res_cache_file:
                res_cache_file.write(' '.join(a) + '\n')
                res_cache_file.close()
            os.chmod(cacheName, 0o755)

### SIMULTED ONSOURCE RESIDUALS DATA WRITING: FRAMES/MDC_RES.cache
else:
    ### write single frame
    frame_name = "HLV-BTLWNB-{epoch}-{duration}.gwf".format(epoch=int(epoch_padded), duration=int(duration_padded))
    frame_path = os.path.join(args.output_path, frame_name)

    frame_paths.append(frame_path)
    pycbc.frame.frame.write_frame(frame_path, args.output_channel, residuals)
    os.chmod(frame_path, 0o755)

    ### MAKE MDC_RES.cache
    a = os.popen(f'ls {args.output_path}/*.gwf | lal_path2cache').read()
    lines = a.strip('\n').split('\n')
    cache_path = os.path.join('datafind', 'MDC_RES.cache')
    with open(os.path.join('datafind', 'MDC_RES.cache'), 'w+') as MDC_cache:
        for l in lines:
            e = l.split(' ')
            ### transferred path of frame file; relative pathing given creation of MDC_RES.cache on execute nodes
            e[4] = '../' + os.path.join('residual_frame', e[4].split('localhost')[-1].split('/')[-1])
            MDC_cache.write(' '.join(e) + '\n')
        MDC_cache.close()

if args.make_plots:
    print("making diagnostic plots")

    #
    # Filter (bandpass and notches)
    #
    print("Filtering")
    delta_t = waveforms[0].delta_t
    filterBas = filt.get_filt_coefs(waveforms[0].sample_rate, 30, 300, False, False)
    filtered_data = [
        pycbc.types.TimeSeries((filt.filter_data(d.data, filterBas)), epoch=epoch_padded, delta_t=delta_t) for d in
        data]
    filtered_residuals = [
        pycbc.types.TimeSeries((filt.filter_data(d.data, filterBas)), epoch=epoch_padded, delta_t=delta_t) for d in
        residuals]
    filtered_waveforms = [
        pycbc.types.TimeSeries((filt.filter_data(d.data, filterBas)), epoch=epoch_padded, delta_t=delta_t) for
        d in waveforms]

    times = waveforms[0].sample_times - args.trigtime
    
    ### plot time series 
    f, ax = plt.subplots(nrows=len(ifos), figsize=(10,6*len(ifos)))

    for i in range(len(ifos)):
        if not args.sim_onsource_residuals:
            dat_label = 'Data'
        else:
            dat_label = 'MDC WF'
        ax[i].plot(times, filtered_data[i], label=dat_label)
        ax[i].plot(times, filtered_residuals[i], label='Residuals')
        ax[i].plot(times, filtered_waveforms[i], label='ML PE Waveform')
        ax[i].set_xlim(-0.2, 0.2)
        ax[i].set_title(ifos[i])
        ax[i].set_xlabel('Time from trigger [s]')
        ### using same ylim for all detectors
        if ifos[i] == 'V1':
            ax[i].set_ylim(-5e-22, 5e-22)
        else:
            ax[i].set_ylim(-5e-22, 5e-22)
    ax[0].legend(loc='upper left')
    f.tight_layout()
    f.savefig(os.path.join(args.output_path, 'residuals_diagnostics_TD.png'))
    
    ### TD plots over full seglen - time domain
    if args.cust_bw_seglen:
        beg_lim =  -1 * (args.cust_bw_seglen / 2 )
        end_lim =  args.cust_bw_seglen / 2
    else:
        beg_lim =  -1 * (args.duration / 2 )
        end_lim =   args.duration / 2
    for i in range(len(ifos)):
        ax[i].set_xlim(beg_lim, end_lim)
    f.savefig(os.path.join(args.output_path, f'residuals_diagnostics_TD_fullseglen.png'))
    plt.close('all')
    
    ### plotting frequency series
    f, ax = plt.subplots(nrows=len(ifos), figsize=(10, 6*len(ifos)))
    fftnorm = np.sqrt(2. / args.srate)
    for i in range(len(ifos)):
        dataF = filtered_data[i].to_frequencyseries() / fftnorm
        waveformF = filtered_waveforms[i].to_frequencyseries() / fftnorm
        residualsF = filtered_residuals[i].to_frequencyseries() / fftnorm

        ax[i].loglog(dataF.sample_frequencies, abs(dataF),
                    label='Data')
        ax[i].loglog(waveformF.sample_frequencies, abs(waveformF),
                    label='ML PE Waveform')
        ax[i].loglog(residualsF.sample_frequencies, abs(residualsF),
                    label='Residuals')

        ax[i].set_xlabel('Frequency [Hz]')
        ax[i].set_title(ifos[i])
    ax[0].legend(loc='lower left')
    
    f.tight_layout()
    f.savefig(os.path.join(args.output_path, f'residuals_diagnostics_FD.png'))
    plt.close('all')

    ### plotting whitened waveform
    # Adjust times so that zero is on an integer second
    trigtime, delta = divmod(args.trigtime, 1)
    local_times = waveforms_wt[0].sample_times - args.trigtime

    f, ax = plt.subplots(figsize=(4*len(ifos)+2, 4), nrows=1, ncols=len(ifos))

    for i in range(len(ifos)):
        ax[i].plot(local_times, waveforms_wt[i])
        ax[i].set_xlim(-0.075, 0.075)
        ax[i].set_xlabel('Time from %d + %.4f [s]' % (int(trigtime), delta))
        ax[i].set_ylabel('%s Whitened Strain' % ifos[i])
        ax[i].minorticks_on()
    f.savefig(os.path.join(args.output_path, 'whitened_waveform.png'))
    plt.close('all')
    
    if args.make_omega_scans:
        print("Making Omega scans")
        
        f_range = (9, 0.8 * args.srate / 2)
        q_range = (10, 12)
        outseg = (np.floor(args.trigtime - 2), np.floor(args.trigtime + 2))
        qscans_data = {}
        print("Reading in Data")
        for i, ifo in enumerate(ifos):
            print(ifo)
            temp = data[i]
            times = temp.sample_times
            delta_t = temp.delta_t
            temp = iwc.strain.whiten_strain(temp, psds[i])
            temp = TimeSeries(data=np.array(temp.tseries), dt=delta_t, t0=times[0])
            qscans_data[ifo] = temp.q_transform(frange=f_range, qrange=q_range,
                                                outseg=outseg, whiten=False)

        qscans_res = {}
        print("Reading in residuals")
        for i, ifo in enumerate(ifos):
            print(ifo)
            temp = residuals[i] 
            times = temp.sample_times
            delta_t = temp.delta_t
            temp = iwc.strain.whiten_strain(temp, psds[i])
            temp = TimeSeries(data=np.array(temp.tseries), dt=delta_t, t0=times[0])
            qscans_res[ifo] = temp.q_transform(frange=f_range, qrange=q_range,
                                            outseg=outseg, whiten=False)

        qtimes_data = qscans_data[ifos[0]].xindex.value
        qfreqs_data = qscans_data[ifos[0]].yindex.value

        qtimes_res = qscans_res[ifos[0]].xindex.value
        qfreqs_res = qscans_res[ifos[0]].yindex.value
        plt.close()

        fbig, axbig = plt.subplots(figsize=(len(ifos)*8+2, 8),
                                ncols=len(ifos), nrows=2,
                                squeeze=False)

        ifo_names = {'H1': 'Hanford', 'L1': 'Livingston', 'V1': 'Virgo'}

        nanosecond, origin = np.modf(args.trigtime)
        for row in range(2):
            print("on row ", row)
            for col, ifo in enumerate(sorted(qscans_data.keys())):
                if row == 0:
                    print('Plotting data qscans')
                    pcol = axbig[row][col].pcolormesh(qtimes_data - origin, qfreqs_data,
                                                    4.0 / np.pi * qscans_data[ifo].T,
                                                    vmin=0, vmax=25.5,
                                                    cmap='viridis', shading='gouraud',
                                                    rasterized=True)

                    if ifo == 'H1':
                        axbig[row][col].set_ylabel('Data Frequency [Hz]')


                    axbig[row][col].set_ylim(10, 0.75 * args.srate / 2)
                    axbig[row][col].minorticks_on()
                    axbig[row][col].set_yscale('log')
                    axbig[row][col].set_title('%s' % ifo_names[ifo])

                if row == 1:
                    print('Plotting res qscans')
                    pcol = axbig[row][col].pcolormesh(qtimes_res - origin, qfreqs_res,
                                                    4.0 / np.pi * qscans_res[ifo].T,
                                                    vmin=0, vmax=25.5,
                                                    cmap='viridis', shading='gouraud',
                                                    rasterized=True)
                    if ifo == 'H1':
                        axbig[row][col].set_ylabel('Residual Frequency [Hz]')

                    axbig[row][col].set_ylim(10, 0.75 * args.srate / 2)
                    axbig[row][col].minorticks_on()
                    axbig[row][col].set_yscale('log')
                    axbig[row][col].set_xlabel('Time [s]')

                axbig[row][col].set_xlim(-0.4 + nanosecond + 0.02, 0.025 + nanosecond + 0.20)

        plt.subplots_adjust(left=0.075, right=0.925, bottom=0.09, top=0.95,
                            wspace=0.1, hspace=0.1)

        cbaxes = fbig.add_axes([.93, 0.2, 0.015, 0.7])
        cbar = fbig.colorbar(pcol,
                            orientation='vertical',
                            cax=cbaxes)
        for axis in ['top', 'bottom', 'left', 'right']:
            cbaxes.spines[axis].set_linewidth(0.25)

        cbar.set_label('Normalized Energy')
        cbar.ax.xaxis.set_label_position('top')
        print('Saving')
        fbig.savefig(os.path.join(args.output_path, 'white_data_res.png'))

    # if __name__ == "__main__":
    #     #
    #     # Parse Configs
    #     #
    #     print('executing now')
    #     args = parser()
    #     try:
    #         main(args)
    #     except Exception as e:
    #         if opts.intermediate_run:
    #             sys.exit(0)
    #         else:
    #             raise e
