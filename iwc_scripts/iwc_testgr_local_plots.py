import sys
import os

import numpy as np

import matplotlib
matplotlib.use("Agg")
from matplotlib import pyplot as plt

plt.rc('text', usetex=True)


# plt.rcParams.update({
# "text.usetex": True,
# "font.family": "sans-serif",
# "font.sans-serif": ["Helvetica"]})

import glob
import traceback
import argparse

### dev functions for testing outputs
def load(path):
    f = open(path, 'rb' )
    return pickle.load(f)

def save(df, path):
    f = open(path, 'wb' )
    pickle.dump(df,f)
    return

def parser():

    parser = argparse.ArgumentParser(description=__doc__,
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--workdir", type=str, required=True, help='Path to testgr_output directory to dump plots into. Assuming saved analysis data into npz file in testgr_output.')

    return parser.parse_args()


### call opts; different than args (which we load)
opts = parser()


### if pass relative path; absolutely path
if opts.workdir == './testgr_output':
    opts.workdir = os.path.join(os.getcwd(), 'testgr_output')   
elif opts.workdir == '.':
    opts.workdir = os.getcwd()

if 'residual_frame' not in opts.workdir:
    raise IOError('Make sure that a path to an testgr/testgr_output directory is passed.')
    
## now check to make sure path exists
if not os.path.exists(opts.workdir):
    raise IOError('Please enter a valid path to a testgr/testgr_output directory.')


### hack lifted from stack exchange to allow for pickled loading
# np_load_old = np.load

# modify the default parameters of np.load
# np.load = lambda *a,**k: np_load_old(*a, allow_pickle=True, **k)
tgr_file = os.path.join(opts.workdir, 'testgr-analy-dat.p')
try:
    with open(tgr_file) as fp:
        print(f'TGR file {tgr_file} is readable. Proceeding with plot generation')


    except IOError as err:
        print(f'TGR file {tgr_file} is NOT readable. Make sure output was good from TGR job.')
        print('\nFull Error Trace:')
        print(err)
        sys.exit(1)


loaded_data = load(tgr_file)
globals().update(loaded_data)

args.output_path = opts.workdir


print('TestGR Data Loaded\n--------------------------')

print("Making TGR Reporting Plots")


### MARK DEV: Deprecate Bayes Factor usage in Residuals test
# if 'BF' in opts.comparison_statistic:
#     f, ax = plt.subplots()
#     p = ax.plot(offsource_logBsn_sorted, logBsn_ecdf, label='signal-to-noise')
#     if opts.onsource_path is not None:
#         ax.axvline(onsource_logBsn, linestyle='--', color=p[0].get_color())

#     p = ax.plot(offsource_logBsg_sorted, logBsg_ecdf, label='signal-to-glitch')
#     if opts.onsource_path is not None:
#         ax.axvline(onsource_logBsg, linestyle='--', color=p[0].get_color())
#     ax.set_xlabel(r'log B')
#     ax.legend(loc='lower right')

#     ax.set_ylim(0, 1)
#     ax.minorticks_on()
#     ax.set_ylabel('CDF')
#     ax.grid(linestyle='-', color='grey')

#     f.tight_layout()

#     f.savefig(os.path.join(opts.output_path, 'logB_residuals.png'))
#     f.savefig(os.path.join(opts.output_path, 'logB_residuals.pdf'))

### MARK DEV: Deprecate Bayes Factor usage in Residuals test
# if 'SNR' in opts.comparison_statistic:

f, ax = plt.subplots(nrows=2,
                        figsize=(fig_size[0] * 0.5 * len(ifos), 1.4 * fig_size[1] * 0.5 * len(ifos)))
ax[0].hist(onsource_netsnr, bins=100, alpha=1,
            label=r'$\textrm{Network SNR}$', histtype='stepfilled')
for i in range(len(ifos)):
    ax[0].hist(onsource_ifosnrs[i], bins=100, label=r"$%s_{\textrm{SNR}}$" % ifos[i],
                histtype='stepfilled', alpha=0.75)
ax[0].axvline(SNRintervals[2], label=r'$SNR_{90}$', color='k', lw=8.0)

ax[0].legend(loc='upper right')
ax[0].set_xlabel(r'$\textrm{Signal-to-Noise Ratio}$', color='k', fontsize=fontsize)
ax[0].set_ylabel(r'$\textrm{PDF}$', color='k', fontsize=fontsize)
ax[0].minorticks_on()

ax[1].hist(FFdist, bins=100, histtype='stepfilled')
ax[1].axvline(FFintervals[0], label=r'$\textrm{90\% L.L.}$', linestyle='--',
                color='k')
ax[1].axvline(FFintervals[1], label=r'$\textrm{Median}$', linestyle='-',
                color='k')
ax[1].axvline(FFintervals[2], linestyle='--', color='k')
ax[1].set_xlabel(r'$\textrm{Fitting Factor}$', color='k', fontsize=fontsize)
ax[1].set_ylabel(r'$\textrm{PDF}$', color='k', fontsize=fontsize)
ax[1].legend(loc='upper left', fontsize=fontsize)
ax[1].minorticks_on()

f.tight_layout()

# f.savefig(os.path.join(opts.output_path, 'residual-snr_FF-distributions.pdf'))
f.savefig(os.path.join(opts.output_path, 'residual-snr_FF-distributions.png'))

f, ax = plt.subplots()

ax.plot(offsource_snr90_sorted, 1 - snr90_ecdf, lw=4.0, label=r'Offsource $SNR_{90}$')
ax.set_xlabel(r'$SNR_{90}$')

ax.set_ylim(0, 1)
ax.minorticks_on()
ax.set_ylabel('1 - CDF')
ax.grid(linestyle='-', color='grey')
if opts.onsource_path is not None:
    ax.axvline(onsource_snr90, linestyle='--', lw=4.0, color='red', label=r'Onsource $SNR_{90}$')
    props = dict(boxstyle='round', facecolor='grey', alpha=0.5)
    textstr = 'p-value = %.2f' % p_snr90
    # place a text box in upper left in axes coords
    ax.text(0.05, 0.95, textstr, transform=ax.transAxes, fontsize=fontsize,
            verticalalignment='top', bbox=props)

### MARK DEV: add SNR GR duck db or some such

ax.legend(loc='upper right', fontsize=fontsize)

f.tight_layout()

plt.savefig(os.path.join(opts.output_path, 'snr_residuals.png'))
plt.close('all')


# print("Filtering")

# filterBas = filt.get_filt_coefs(data[0].sample_rate, 30, 300, False, False)
# filtered_data = [
#     pycbc.types.TimeSeries((filt.filter_data(d.data, filterBas)), epoch=epoch_padded, delta_t=delta_t) for d in
#     data]
# filtered_residuals = [
#     pycbc.types.TimeSeries((filt.filter_data(d.data, filterBas)), epoch=epoch_padded, delta_t=delta_t) for d in
#     residuals]
# filtered_waveforms = [
#     pycbc.types.TimeSeries((filt.filter_data(d.data, filterBas)), epoch=epoch_padded, delta_t=delta_t) for
#     d in waveforms]

# # T-domain
# times = waveforms[0].sample_times - args.trigtime


# f, ax = plt.subplots(nrows=len(ifos), figsize=(10,6*len(ifos)))

# for i in range(len(ifos)):
#     ax[i].plot(times, filtered_data[i], label='data')
#     ax[i].plot(times, filtered_waveforms[i], label='model')
#     ax[i].plot(times, filtered_residuals[i], label='residuals')
#     # ax[i].set_ylim(-4,4)
#     ax[i].set_xlim(-0.2, 0.2)
#     ax[i].set_title(ifos[i])
#     ax[i].set_xlabel('Time from trigger [s]')
#     if ifos[i] == 'V1':
#         ax[i].set_ylim(-5e-21, 5e-21)
#     else:
#         ax[i].set_ylim(-1e-21, 1e-21)
# ax[0].legend(loc='upper left')

# f.tight_layout(rect=[0, 0.03, 1, 0.95]) ### O4: Missing font libraries
# plt.savefig(os.path.join(args.output_path, 'residuals_diagnostics_TD.png'))

# # ASD colored data
# plt.close('all')

# f, ax = plt.subplots(nrows=len(ifos), figsize=(10, 6*len(ifos)))
# fftnorm = np.sqrt(2. / args.srate)
# for i in range(len(ifos)):
#     dataF = filtered_data[i].to_frequencyseries() / fftnorm
#     waveformF = filtered_waveforms[i].to_frequencyseries() / fftnorm
#     residualsF = filtered_residuals[i].to_frequencyseries() / fftnorm

#     ax[i].loglog(dataF.sample_frequencies, abs(dataF),
#                     label='Data')
#     ax[i].loglog(waveformF.sample_frequencies, abs(waveformF),
#                     label='Model')
#     ax[i].loglog(residualsF.sample_frequencies, abs(residualsF),
#                     label='Residuals')

#     ax[i].set_xlabel(f'Frequency [Hz]')
#     ax[i].set_title(ifos[i])
# ax[0].legend(loc='lower left')
# # f.tight_layout()
# f.tight_layout(rect=[0, 0.03, 1, 0.95])

# plt.savefig(os.path.join(args.output_path, 'residuals_diagnostics_FD.png'))
# plt.close('all')

# if args.psd_files is not None:

#     # Adjust times so that zero is on an integer second
#     trigtime, delta = divmod(args.trigtime, 1)
#     local_times = waveforms_wt[0].sample_times - args.trigtime

#     f, ax = plt.subplots(figsize=(4*len(ifos)+2, 4), nrows=1, ncols=len(ifos))

#     for i in range(len(ifos)):
#         ax[i].plot(local_times, waveforms_wt[i])
#         ax[i].set_xlim(-0.075, 0.075)
#         ax[i].set_xlabel('Time from %d + %.4f [s]' % (int(trigtime), delta))
#         ax[i].set_ylabel('%s Whitened Strain' % ifos[i])
#         ax[i].minorticks_on()

#     plt.savefig(os.path.join(args.output_path, 'whitened_waveform.png'))
#     if args.make_omega_scans:
#         if not args.frame_cache:
#             raise ArgumentError('pass --frame-cache in addition to --omega-scans ')
#         print("Making Omega scans")
        
#         ### MARK DEV: TODO: custom flag for q-resolution based on signal? e
#         f_range = (9, 0.8 * args.srate / 2)
#         q_range = (10, 12)
#         outseg = (np.floor(args.trigtime - 2), np.floor(args.trigtime + 2))
#         qscans_data = {}
#         print("Reading in Data")
#         for i, ifo in enumerate(ifos):
#             print(ifo)

#             temp = TS.read(inFrames[i], args.input_channel[i], epoch_padded, 
#                         epoch_padded + duration_padded, format='gwf').to_pycbc()
#             # OLD:   
#             # temp = pycbc.frame.read_frame(args.frame_cache[i], args.input_channel[i]
#             #                               , args.trigtime - 2, args.trigtime + 2)
#             # temp = pycbc.frame.read_frame(args.frame_cache[i], args.input_channel[i]
#             #                               , np.floor(args.trigtime - 2), np.floor(args.trigtime + 2))
            

                
#             # temp = TS.fetch(f"{0}".format(args.input_channel[i]),
#             # epoch_padded, epoch_padded + duration_padded, verbose=True)
#             # data = [TS.fetch("{0}:{1}".format(ifo, args.input_channel[i]),
#             # epoch_padded, epoch_padded + duration_padded, verbose=True) for i, ifo in enumerate(ifos)]

#             temp = resample_to_delta_t(temp, 1. / args.srate)
#             times = temp.sample_times
#             delta_t = temp.delta_t
#             temp = iwc.strain.whiten_strain(temp, psds[i])
#             temp = TS(data=np.array(temp.tseries), dt=delta_t, t0=times[0]) ### MARK DEV: don't know that we need this 
#             qscans_data[ifo] = temp.q_transform(frange=f_range, qrange=q_range,
#                                                 outseg=outseg, whiten=False)

#         qscans_res = {}
#         print("Reading in residuals")
#         for i, ifo in enumerate(ifos):
#             print(ifo)
#             # temp = pycbc.frame.read_frame(frame_paths[i], args.output_channel[i]
#             #                               , np.floor(args.trigtime - 2), np.floor(args.trigtime + 2))
#             # temp = resample_to_delta_t(temp, 1. / args.srate)
            
#             temp = residuals[i] ### sub in residuals here from above; convert to time series 
#             times = temp.sample_times
#             delta_t = temp.delta_t
#             temp = iwc.strain.whiten_strain(temp, psds[i])
#             temp = TS(data=np.array(temp.tseries), dt=delta_t, t0=times[0])
#             qscans_res[ifo] = temp.q_transform(frange=f_range, qrange=q_range,
#                                                 outseg=outseg, whiten=False)

#         qtimes_data = qscans_data[ifos[0]].xindex.value
#         qfreqs_data = qscans_data[ifos[0]].yindex.value

#         qtimes_res = qscans_res[ifos[0]].xindex.value
#         qfreqs_res = qscans_res[ifos[0]].yindex.value
#         plt.close()

#         fbig, axbig = plt.subplots(figsize=(len(ifos)*8+2, 8),
#                                     ncols=len(ifos), nrows=2,
#                                     squeeze=False)

#         ifo_names = {'H1': 'Hanford', 'L1': 'Livingston', 'V1': 'Virgo'}

#         nanosecond, origin = np.modf(args.trigtime)
#         for row in range(2):
#             print("on row ", row)
#             for col, ifo in enumerate(sorted(qscans_data.keys())):
#                 if row == 0:
#                     print('Plotting data qscans')
#                     pcol = axbig[row][col].pcolormesh(qtimes_data - origin, qfreqs_data,
#                                                         4.0 / np.pi * qscans_data[ifo].T,
#                                                         vmin=0, vmax=25.5,
#                                                         cmap='viridis', shading='gouraud',
#                                                         rasterized=True)

#                     if ifo == 'H1':
#                         axbig[row][col].set_ylabel(f'Data Frequency [Hz]')

#                     axbig[row][col].set_ylim(10, 0.75 * args.srate / 2)
#                     axbig[row][col].minorticks_on()
#                     axbig[row][col].set_yscale('log')
#                     axbig[row][col].set_title('%s' % ifo_names[ifo])

#                 if row == 1:
#                     print('Plotting res qscans')
#                     pcol = axbig[row][col].pcolormesh(qtimes_res - origin, qfreqs_res,
#                                                         4.0 / np.pi * qscans_res[ifo].T,
#                                                         vmin=0, vmax=25.5,
#                                                         cmap='viridis', shading='gouraud',
#                                                         rasterized=True)
#                     if ifo == 'H1':
#                         axbig[row][col].set_ylabel(f'Residual Frequency [Hz]')

#                     axbig[row][col].set_ylim(10, 0.75 * args.srate / 2)
#                     axbig[row][col].minorticks_on()
#                     axbig[row][col].set_yscale('log')
#                     axbig[row][col].set_xlabel('Time [s]')

#                 axbig[row][col].set_xlim(-0.4 + nanosecond + 0.02, 0.025 + nanosecond + 0.20)

#         plt.subplots_adjust(left=0.075, right=0.925, bottom=0.09, top=0.95,
#                             wspace=0.1, hspace=0.1)

#         cbaxes = fbig.add_axes([.93, 0.2, 0.015, 0.7])
#         cbar = fbig.colorbar(pcol,
#                                 orientation='vertical',
#                                 cax=cbaxes)
#         for axis in ['top', 'bottom', 'left', 'right']:
#             cbaxes.spines[axis].set_linewidth(0.25)

#         cbar.set_label(f'Normalized Energy')
#         cbar.ax.xaxis.set_label_position('top')

#         print('Saving')
#         # fbig.tight_layout(rect=[0, 0.03, 1, 0.95])
#         # plt.savefig(os.path.join(args.output_path, 'white_data_res.pdf'))
#         plt.savefig(os.path.join(args.output_path, 'white_data_res.png'))
