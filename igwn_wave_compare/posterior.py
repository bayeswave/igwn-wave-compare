#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Posterior parsing routines.
"""

from __future__ import print_function
import configparser
from pesummary.gw.file.read import read



### update with maintained package
from ligo.lw import lsctables, ligolw 
from igwn_wave_compare import iwc_io as io
import numpy as np
import os
import logging
import pandas as pd

import pickle


def loglikelihood(freqs, hs, ds, psds, flow=0.):
    logl = 0.

    for h, d, psd in zip(hs, ds, psds):
        df = freqs[1] - freqs[0]
        sel = freqs > flow

        hh = np.sum(4.0 * df * (h[sel].conj() * h[sel]).real / psd[sel])
        dh = np.sum(4.0 * df * (d[sel].conj() * h[sel]).real / psd[sel])
        dd = np.sum(4.0 * df * (d[sel].conj() * d[sel]).real / psd[sel])

        logl += -0.5 * (hh - 2.0 * dh)
        # logl -= np.sum(np.log(2.0*np.pi*psd/(4.0*df)))

    return logl


def parse_header(inp):
    """
    Get past the header of an output file and return column names
    """
    # Check if the header is more complex than just a line of column names
    check_nlines = 15

    simple_header = True
    header = inp.readline().split()
    if header[0] == "#":
        header = header[1:]
    ncol = len(header)
    nlines_read = 0
    while nlines_read < check_nlines:
        nlines_read += 1
        if len(inp.readline().split()) != ncol:
            simple_header = False
            break

    inp.seek(0)
    header = inp.readline().split()
    if header[0] == "#":
        header = header[1:]

    if not simple_header:
        while True:
            if len(header) > 0 and header[0] == 'cycle':
                break
            header = inp.readline().split()
    return [p.lower() for p in header]


def get_logpost(samples):
    """
    Extract the log-posterior series of the chain.
    """
    try:
        logpost = samples['logpost']
    except ValueError:
        try:
            try:
                prior = samples['prior']
                logprior = np.log(prior)
            except:
                try:
                    logprior = samples['logprior']
                except:
                    logprior = samples['log_prior']
            try:
                loglike = samples['logl']
            except:
                loglike = samples['log_likelihood']
            logpost = logprior + loglike
        except ValueError:
            try:
                post = samples['post']
                logpost = np.log(post)
            except:
                logging.warning("Couldn't find a way to get the post column. Returning the log likelihood...")
                try:
                    loglike = samples['logl']
                except:
                    loglike = samples['log_likelihood']
                logpost = loglike
    return logpost


def get_loglike(samples):
    """
    Extract the log-likelihood series of the chain.
    """
    try:
        loglike = samples['logl']
    except:
        loglike = samples['log_likelihood']
    return loglike


def extract_samples(infile, label=None):
    """
    Extract samples from a h5 or an ASCII file output file.
    """
    if os.path.basename(infile).endswith(('.dat', '.txt')):
        samples = np.genfromtxt(infile, names=True)
        return samples, None
    elif os.path.basename(infile).endswith(('.h5', '.hdf5', '.json')):
        if label is None:
            raise RuntimeError('Please provide label along with pesummary file')
        pesumm_file = read(infile)
        samples = pesumm_file.samples_dict[label].to_structured_array()
        return samples, pesumm_file


def extract_map_sample(samples):
    """
    Extract the maximum a posteriori sample from a samples array.
    """
    logpost = get_logpost(samples)
    map_idx = logpost.argmax()

    return samples[map_idx]


def extract_maxl_sample(samples):
    """
    Extract the maximum a posteriori sample from a samples array.
    """
    loglike = get_loglike(samples)
    map_idx = loglike.argmax()

    return samples[map_idx]


def credible_bounds(function_samples, cl=0.90):
    """
    Get the upper and lower credible boundaries of a 1-D function from a sample.
    """
    function_samples = np.atleast_2d(function_samples)
    N = function_samples.shape[0]

    sorted_samples = np.sort(function_samples, axis=0)
    low = sorted_samples[int((1 - cl) / 2. * N), :]
    high = sorted_samples[int((1 + cl) / 2. * N), :]
    return low, high


def extract_calibration_frequencies(samples, ifos, pesumm_file=None, label=None):
    sample_params = samples.dtype.names
    try:
        spcal_freqs = {}
        for ifo in ifos:
            # Get frequencies of spline control points from the samples.
            try:
                freq_params = sorted(
                    [
                        param
                        for param in sample_params
                        if f'{ifo}_spcal_freq' in param
                        or f'{ifo.lower()}_spcal_freq' in param
                    ]
                )
                if len(freq_params) == 0:
                    raise ValueError

                spcal_freqs[ifo] = np.array([samples[param][0] for param in freq_params])
            except ValueError as e:
                # If the pesumm_file and label aren't supplied, there's nothing left to do
                # and we return a None
                if pesumm_file is None or label is None:
                    print('Extract Calibration frequencies: label (expected behavior for O4) or pesummary_file are none;\nCalibration parameters not found')
                    return None
                    
                
                ind = pesumm_file.labels.index(label)
                freq_params = sorted(
                    [
                        param
                        for param in pesumm_file.extra_kwargs[ind][
                            "other"
                        ].keys()
                        if f'{ifo}_spcal_freq' in param
                        or f'{ifo.lower()}_spcal_freq' in param
                    ]
                )
                if len(freq_params) == 0:
                    raise ValueError from e
                spcal_freqs[ifo] = np.array([pesumm_file.extra_kwargs[ind]["other"][param] for param in freq_params])
        return spcal_freqs
    except ValueError:
        return None

# ### MARK DEV: migrate to SimInspiralIO (or more general)
# def create_xml_table_samples(pe_file, pe_approx, pe_flow, pe_amporder, injfile_name, trigtimes=None):
#     ### new format for np
#     sim_inspiral_dt = [
#         ('process_id', 'int64'),
#         ('waveform', 'U64'),
#         ('geocent_end_time', 'int32'),
#         ('geocent_end_time_ns', 'int32'),
#         ('h_end_time', 'int32'),
#         ('h_end_time_ns', 'int32'),
#         ('l_end_time', 'int32'),
#         ('l_end_time_ns', 'int32'),
#         ('g_end_time', 'int32'),
#         ('g_end_time_ns', 'int32'),
#         ('t_end_time', 'int32'),
#         ('t_end_time_ns', 'int32'),
#         ('v_end_time', 'int32'),
#         ('v_end_time_ns', 'int32'),
#         ('end_time_gmst', 'float64'),
#         ('source', 'U64'),
#         ('mass1', 'float64'),
#         ('mass2', 'float64'),
#         ('mchirp', 'float64'),
#         ('eta', 'float64'),
#         ('distance', 'float64'),
#         ('longitude', 'float64'),
#         ('latitude', 'float64'),
#         ('inclination', 'float64'),
#         ('coa_phase', 'float64'),
#         ('polarization', 'float64'),
#         ('psi0', 'float64'),
#         ('psi3', 'float64'),
#         ('alpha', 'float64'),
#         ('alpha1', 'float64'),
#         ('alpha2', 'float64'),
#         ('alpha3', 'float64'),
#         ('alpha4', 'float64'),
#         ('alpha5', 'float64'),
#         ('alpha6', 'float64'),
#         ('beta', 'float64'),
#         ('spin1x', 'float64'),
#         ('spin1y', 'float64'),
#         ('spin1z', 'float64'),
#         ('spin2x', 'float64'),
#         ('spin2y', 'float64'),
#         ('spin2z', 'float64'),
#         ('theta0', 'float64'),
#         ('phi0', 'float64'),
#         ('f_lower', 'float64'),
#         ('f_final', 'float64'),
#         ('eff_dist_h', 'float64'),
#         ('eff_dist_l', 'float64'),
#         ('eff_dist_g', 'float64'),
#         ('eff_dist_t', 'float64'),
#         ('eff_dist_v', 'float64'),
#         ('numrel_mode_min', 'int32'),
#         ('numrel_mode_max', 'int32'),
#         ('numrel_data', 'U64'),
#         ('amp_order', 'int32'),
#         ('taper', 'U64'),
#         ('bandpass', 'int32'),
#         ('simulation_id', 'int64')
#     ]

#     if isinstance(trigtimes, np.ndarray):
#         ninj = trigtimes.shape[0] ##
#         injections = np.zeros((ninj), dtype=sim_inspiral_dt) ##

#         pe_structured_array = np.genfromtxt(pe_file, names=True) ##
#         ### get all samples 
#         post_samples = pd.DataFrame(pe_structured_array).sample(n=ninj) ##
#         ### being (controlled) construction of structured numpy.ndarray
#         trigtimes_ns, trigtimes_s = np.modf(trigtimes) ##
#         trigtimes_ns *= 10 ** 9 ## ### convert decimal part of trigtime to nanoseconds in accordance with SimInspiral conventions

#     elif trigtimes is None:
#         ninj = len(pe_structured_array) ##
#         injections = np.zeros((ninj), dtype=sim_inspiral_dt) ##
#         pe_structured_array = np.genfromtxt(pe_file, names=True) ##
#         post_samples = pd.DataFrame(pe_structured_array) ##
#         try:
#             trigtimes_ns, trigtimes_s = np.modf(post_samples["geocent_time"]) ##
#         except KeyError:
#             trigtimes_ns, trigtimes_s = np.modf(post_samples["time"]) ##
#         trigtimes_ns *= 10 ** 9 ## 
#     else:
#         raise ValueError('Please enter either a valid PE samples file or a numppy.ndarray of trigtimes.')


#     # try:
#     #     trigtimes_ns, trigtimes_s = np.modf(post_samples["geocent_time"])
#     # except KeyError:
#     #     trigtimes_ns, trigtimes_s = np.modf(post_samples["time"])
#     # trigtimes_ns *= 10 ** 9

#     injections["waveform"] = [pe_approx for _ in range(ninj)]
#     injections["taper"] = ["TAPER_NONE" for _ in range(ninj)]
#     injections["f_lower"] = [pe_flow for _ in range(ninj)]
#     try:
#         injections["mchirp"] = np.array(post_samples["chirp_mass"])
#     except KeyError:
#         injections["mchirp"] = np.array(post_samples["mc"])
#     try:
#         injections["eta"] = np.array(post_samples["symmetric_mass_ratio"])
#     except KeyError:
#         injections["eta"] = np.array(post_samples["eta"])
#     try:
#         injections["mass1"] = np.array(post_samples["mass_1"])
#     except KeyError:
#         injections["mass1"] = np.array(post_samples["m1"])
#     try:
#         injections["mass2"] = np.array(post_samples["mass_2"])
#     except KeyError:
#         injections["mass2"] = np.array(post_samples["m2"])
#     injections["geocent_end_time"] = trigtimes_s
#     injections["geocent_end_time_ns"] = trigtimes_ns
#     try:
#         injections["distance"] = np.array(post_samples["luminosity_distance"])
#     except KeyError:
#         injections["distance"] = np.array(post_samples["dist"])
#     injections["longitude"] = np.array(post_samples["ra"])
#     injections["latitude"] = np.array(post_samples["dec"])
#     injections["inclination"] = np.array(post_samples["iota"])
#     injections["coa_phase"] = np.array(post_samples["phase"])
#     injections["polarization"] = np.array(post_samples["psi"])
#     try:
#         injections["spin1x"] = np.array(post_samples["spin_1x"])
#     except KeyError:
#         injections["spin1x"] = np.array(post_samples["a1"]) * np.sin(np.array(post_samples["theta1"])) * np.cos(
#             np.array(post_samples["phi1"]))
#     try:
#         injections["spin1y"] = np.array(post_samples["spin_1y"])
#     except KeyError:
#         injections["spin1y"] = np.array(post_samples["a1"]) * np.sin(np.array(post_samples["theta1"])) * np.sin(
#             np.array(post_samples["phi1"]))
#     try:
#         injections["spin1z"] = np.array(post_samples["spin_1z"])
#     except KeyError:
#         injections["spin1z"] = np.array(post_samples["a1"]) * np.cos(np.array(post_samples["theta1"]))
#     try:
#         injections["spin2x"] = np.array(post_samples["spin_2x"])
#     except KeyError:
#         injections["spin2x"] = np.array(post_samples["a2"]) * np.sin(np.array(post_samples["theta2"])) * np.cos(
#             np.array(post_samples["phi2"]))
#     try:
#         injections["spin2y"] = np.array(post_samples["spin_2y"])
#     except KeyError:
#         injections["spin2y"] = np.array(post_samples["a2"]) * np.sin(np.array(post_samples["theta2"])) * np.sin(
#             np.array(post_samples["phi2"]))
#     try:
#         injections["spin2z"] = np.array(post_samples["spin_2z"])
#     except KeyError:
#         injections["spin2z"] = np.array(post_samples["a2"]) * np.cos(np.array(post_samples["theta2"]))
#     injections["amp_order"] = [pe_amporder for _ in range(ninj)]
#     injections["numrel_data"] = ["" for _ in range(ninj)]

#     ### updated for new LIGOLW XML file format
#     inds = np.array(list(range(ninj)))
#     zeros = np.zeros([ninj])

#     injections["process_id"] = zeros # np.array(inds)
#     injections["simulation_id"] = inds

#     ### fill in unused cols with 0's
#     for column, dtype in sim_inspiral_dt:
#         if column not in injections.dtype.names:
#             injections[column] = np.zeros(ninj, dtype=dtype)

#     ### write SimInspiralTable
#     sim_inspiral = io.SimInspiralIO.makeXML(injections, name=injfile_name)
#     return sim_inspiral
    
# def create_xml_table_ML(pe_file, pe_approx, pe_flow, pe_amporder, injfile_name, trigtimes=None):
#     ### new format for np
#     sim_inspiral_dt = [
#         ('process_id', 'int64'),
#         ('waveform', 'U64'),
#         ('geocent_end_time', 'int32'),
#         ('geocent_end_time_ns', 'int32'),
#         ('h_end_time', 'int32'),
#         ('h_end_time_ns', 'int32'),
#         ('l_end_time', 'int32'),
#         ('l_end_time_ns', 'int32'),
#         ('g_end_time', 'int32'),
#         ('g_end_time_ns', 'int32'),
#         ('t_end_time', 'int32'),
#         ('t_end_time_ns', 'int32'),
#         ('v_end_time', 'int32'),
#         ('v_end_time_ns', 'int32'),
#         ('end_time_gmst', 'float64'),
#         ('source', 'U64'),
#         ('mass1', 'float64'),
#         ('mass2', 'float64'),
#         ('mchirp', 'float64'),
#         ('eta', 'float64'),
#         ('distance', 'float64'),
#         ('longitude', 'float64'),
#         ('latitude', 'float64'),
#         ('inclination', 'float64'),
#         ('coa_phase', 'float64'),
#         ('polarization', 'float64'),
#         ('psi0', 'float64'),
#         ('psi3', 'float64'),
#         ('alpha', 'float64'),
#         ('alpha1', 'float64'),
#         ('alpha2', 'float64'),
#         ('alpha3', 'float64'),
#         ('alpha4', 'float64'),
#         ('alpha5', 'float64'),
#         ('alpha6', 'float64'),
#         ('beta', 'float64'),
#         ('spin1x', 'float64'),
#         ('spin1y', 'float64'),
#         ('spin1z', 'float64'),
#         ('spin2x', 'float64'),
#         ('spin2y', 'float64'),
#         ('spin2z', 'float64'),
#         ('theta0', 'float64'),
#         ('phi0', 'float64'),
#         ('f_lower', 'float64'),
#         ('f_final', 'float64'),
#         ('eff_dist_h', 'float64'),
#         ('eff_dist_l', 'float64'),
#         ('eff_dist_g', 'float64'),
#         ('eff_dist_t', 'float64'),
#         ('eff_dist_v', 'float64'),
#         ('numrel_mode_min', 'int32'),
#         ('numrel_mode_max', 'int32'),
#         ('numrel_data', 'U64'),
#         ('amp_order', 'int32'),
#         ('taper', 'U64'),
#         ('bandpass', 'int32'),
#         ('simulation_id', 'int64')
#     ]

#     if isinstance(trigtimes, np.ndarray):
#         ninj = trigtimes.shape[0] ##
#         injections = np.zeros((ninj), dtype=sim_inspiral_dt) ##

#         pe_structured_array = np.genfromtxt(pe_file, names=True) ##
#         post_samples = pd.DataFrame(pe_structured_array) ##

#         maxLVals_df = extract_maxl_sample(post_file_df)
#         post_ML_df = pd.DataFrame(np.repeat(maxLVals_df.values, ninj, axis=0))
#         # post_samples = pd.DataFrame(pe_structured_array).sample(n=ninj)

#         ### use trigtimes that are passed in 
#         trigtimes_ns, trigtimes_s = np.modf(trigtimes)
#         trigtimes_ns *= 10 ** 9

#     elif trigtimes is None:
#         ninj = len(pe_structured_array) ##
#         injections = np.zeros((ninj), dtype=sim_inspiral_dt) ##

#         pe_structured_array = np.genfromtxt(pe_file, names=True) ##
#         post_file_df = pd.DataFrame(pe_structured_array) ## 

#         maxLVals_df = extract_maxl_sample(post_file_df)
#         post_ML_df = pd.DataFrame(np.repeat(maxLVals_df.values, ninj, axis=0))
#         ### use trigtimes from samples file if we don't have trigtimes (as in old implementation), fill in everything else from post_ML_df (maxL values)
#         try:
#             trigtimes_ns, trigtimes_s = np.modf(post_file_df["geocent_time"])
#         except KeyError:
#             trigtimes_ns, trigtimes_s = np.modf(post_file_df["time"])
#         trigtimes_ns *= 10 ** 9
#     else:
#         raise ValueError('Please enter either a valid PE samples file or a numppy.ndarray of trigtimes.')


#     ### OLD ### 
#     ### get maximum likelihood parameters estimate from bilby 

#     #try:
#     #    idx = np.argmax(post_file_df['logl'])
#     #except:
#     #    idx = np.argmax(post_file_df['log_likelihood'])
#     #maxLVals_df = post_file_df.iloc[[idx]]

#     ### duplicate the row ninj times for injection 

#     ### format XML injection

#     injections["geocent_end_time"] = trigtimes_s
#     injections["geocent_end_time_ns"] = trigtimes_ns

#     injections["waveform"] = [pe_approx for _ in range(ninj)]
#     injections["taper"] = ["TAPER_NONE" for _ in range(ninj)]
#     injections["f_lower"] = [pe_flow for _ in range(ninj)]
#     try:
#         injections["mchirp"] = np.array(post_ML_df["chirp_mass"])
#     except KeyError:
#         injections["mchirp"] = np.array(post_ML_df["mc"])
#     try:
#         injections["eta"] = np.array(post_ML_df["symmetric_mass_ratio"])
#     except KeyError:
#         injections["eta"] = np.array(post_ML_df["eta"])
#     try:
#         injections["mass1"] = np.array(post_ML_df["mass_1"])
#     except KeyError:
#         injections["mass1"] = np.array(post_ML_df["m1"])
#     try:
#         injections["mass2"] = np.array(post_ML_df["mass_2"])
#     except KeyError:
#         injections["mass2"] = np.array(post_ML_df["m2"])

#     try:
#         injections["distance"] = np.array(post_ML_df["luminosity_distance"])
#     except KeyError:
#         injections["distance"] = np.array(post_ML_df["dist"])
#     injections["longitude"] = np.array(post_ML_df["ra"])
#     injections["latitude"] = np.array(post_ML_df["dec"])
#     injections["inclination"] = np.array(post_ML_df["iota"])
#     injections["coa_phase"] = np.array(post_ML_df["phase"])
#     injections["polarization"] = np.array(post_ML_df["psi"])
#     try:
#         injections["spin1x"] = np.array(post_ML_df["spin_1x"])
#     except KeyError:
#         injections["spin1x"] = np.array(post_ML_df["a1"]) * np.sin(np.array(post_ML_df["theta1"])) * np.cos(
#             np.array(post_ML_df["phi1"]))
#     try:
#         injections["spin1y"] = np.array(post_ML_df["spin_1y"])
#     except KeyError:
#         injections["spin1y"] = np.array(post_ML_df["a1"]) * np.sin(np.array(post_ML_df["theta1"])) * np.sin(
#             np.array(post_ML_df["phi1"]))
#     try:
#         injections["spin1z"] = np.array(post_ML_df["spin_1z"])
#     except KeyError:
#         injections["spin1z"] = np.array(post_ML_df["a1"]) * np.cos(np.array(post_ML_df["theta1"]))
#     try:
#         injections["spin2x"] = np.array(post_ML_df["spin_2x"])
#     except KeyError:
#         injections["spin2x"] = np.array(post_ML_df["a2"]) * np.sin(np.array(post_ML_df["theta2"])) * np.cos(
#             np.array(post_ML_df["phi2"]))
#     try:
#         injections["spin2y"] = np.array(post_ML_df["spin_2y"])
#     except KeyError:
#         injections["spin2y"] = np.array(post_ML_df["a2"]) * np.sin(np.array(post_ML_df["theta2"])) * np.sin(
#             np.array(post_ML_df["phi2"]))
#     try:
#         injections["spin2z"] = np.array(post_ML_df["spin_2z"])
#     except KeyError:
#         injections["spin2z"] = np.array(post_ML_df["a2"]) * np.cos(np.array(post_ML_df["theta2"]))
#     injections["amp_order"] = [pe_amporder for _ in range(ninj)]
#     injections["numrel_data"] = ["" for _ in range(ninj)]

#     ### updated for new LIGOLW XML file format
#     inds = np.array(list(range(ninj)))
#     zeros = np.zeros([ninj])
#     injections["process_id"] = zeros
#     injections["simulation_id"] = inds

#     ### fill in unused cols with 0's
#     for column, dtype in sim_inspiral_dt:
#         if column not in injections.dtype.names:
#             injections[column] = np.zeros(ninj, dtype=dtype)

#     sim_inspiral = io.SimInspiralIO.makeXML(injections, name=injfile_name)
#     return sim_inspiral
    
    
    
    
    
    
    
    
# def create_xml_table_bounds(bounds_file, injfile_name, realTrigtimes=None):
#     '''Pretty basic functionality thus far: only making injections based on bounds
#     FIXME: add functionality for fixed ra, dec, etc. if config opt does not split. 
#     '''
    
#     ### new format for np
#     sim_inspiral_dt = [
#         ('process_id', 'int64'),
#         ('waveform', 'U64'),
#         ('geocent_end_time', 'int32'),
#         ('geocent_end_time_ns', 'int32'),
#         ('h_end_time', 'int32'),
#         ('h_end_time_ns', 'int32'),
#         ('l_end_time', 'int32'),
#         ('l_end_time_ns', 'int32'),
#         ('g_end_time', 'int32'),
#         ('g_end_time_ns', 'int32'),
#         ('t_end_time', 'int32'),
#         ('t_end_time_ns', 'int32'),
#         ('v_end_time', 'int32'),
#         ('v_end_time_ns', 'int32'),
#         ('end_time_gmst', 'float64'),
#         ('source', 'U64'),
#         ('mass1', 'float64'),
#         ('mass2', 'float64'),
#         ('mchirp', 'float64'),
#         ('eta', 'float64'),
#         ('distance', 'float64'),
#         ('longitude', 'float64'),
#         ('latitude', 'float64'),
#         ('inclination', 'float64'),
#         ('coa_phase', 'float64'),
#         ('polarization', 'float64'),
#         ('psi0', 'float64'),
#         ('psi3', 'float64'),
#         ('alpha', 'float64'),
#         ('alpha1', 'float64'),
#         ('alpha2', 'float64'),
#         ('alpha3', 'float64'),
#         ('alpha4', 'float64'),
#         ('alpha5', 'float64'),
#         ('alpha6', 'float64'),
#         ('beta', 'float64'),
#         ('spin1x', 'float64'),
#         ('spin1y', 'float64'),
#         ('spin1z', 'float64'),
#         ('spin2x', 'float64'),
#         ('spin2y', 'float64'),
#         ('spin2z', 'float64'),
#         ('theta0', 'float64'),
#         ('phi0', 'float64'),
#         ('f_lower', 'float64'),
#         ('f_final', 'float64'),
#         ('eff_dist_h', 'float64'),
#         ('eff_dist_l', 'float64'),
#         ('eff_dist_g', 'float64'),
#         ('eff_dist_t', 'float64'),
#         ('eff_dist_v', 'float64'),
#         ('numrel_mode_min', 'int32'),
#         ('numrel_mode_max', 'int32'),
#         ('numrel_data', 'U64'),
#         ('amp_order', 'int32'),
#         ('taper', 'U64'),
#         ('bandpass', 'int32'),
#         ('simulation_id', 'int64')
#     ]

#     # ninj = trigtimes.shape[0]



#     ### read .ini file that contains bounds
#     config = configparser.ConfigParser()
#     config.read(bounds_file)
#     ### set trigtimes to trigtime list if one is passed from iwc_pipe
#     if realTrigtimes is not None and isinstance(realTrigtimes, np.ndarray):
#         trigtimes = realTrigtimes
#     ### otherwise we're going simulated on trigtimes over an interval
#     elif realTrigtimes is None:
#         tstart = float(config.get('run_params', 'tstart'))
#         tend = float(config.get('run_params', 'tend'))
#         tstep = float(config.get('run_params', 'tstep'))
#         tCheck = (tend - tstart) / tstep

#         if tCheck % 1 == 0:
#             ninj = tCheck
#             trigtimes = np.arange(tstart, tend, tstep)
#         else: 
#             raise ValueError('Please enter a duration in your config file that is an integer multiple of tstep.')
#     else:
#         raise ValueError("Please pass None (simulated trigtimes) and configure tstart, tend, and tstep in injection bounds_file\nor:\npass a numpy.ndarray of real trigtimes as found in iwc_pipe ")

#     ### make injections structured np array    
#     injections = np.zeros((ninj), dtype=sim_inspiral_dt)

#     ### load params from bounds_file config file (.ini)
#     param_bounds = {}
#     param_samples = {}
#     for param, dtype in sim_inspiral_dt:
#         if param in config['source_params']:
#             ### read bounds
#             bounds = config.get('source_params', param)
#             lower, upper = map(float, bounds.split(','))
#             ### sample between the bounds read for ninj injections
#             param_samples[param] = np.random.uniform(lower, upper, size=ninj, dtype=dtype)
#             param_bounds[param] = (lower, upper)
#         else:
#             # Handle case when parameter is not found in the INI file
#             param_bounds[param] = (0.0, 0.0)
#             param_samples[param] = np.zeros(ninj, dtype=dtype)

#     ### turn parameter samples into df
#     injections = pd.DataFrame(param_samples)

#     ### updated indexes for new LIGOLW XML file format
#     inds = np.array(list(range(ninj)))
#     zeros = np.zeros([ninj])
#     injections["process_id"] = zeros # np.array(inds)
#     injections["simulation_id"] = inds

#     ### these can be set here globally & from metadata

#     ### split into integer and fractional part
#     trigtimes_ns, trigtimes_s = np.modf(trigtimes)
#     injections["geocent_end_time"] = trigtimes_s
#     injections["geocent_end_time_ns"] = trigtimes_ns * 10 ** 9
#     # trigtimes_ns *=  ### FIXME: ???

#     ### PASSED CONFIG THINGS
#     approxim = str(config.get('run_params', 'approximant'))
#     flow = float(config.get('run_params', 'flow')) ### technically an analysis one...
#     amporder = float(config.get('run_params', 'amporder'))
#     taper = str(config.get('run_params', 'taper'))
#     ### give to injections df
#     injections["waveform"] = [approxim for _ in range(ninj)]
#     injections["taper"] = [taper for _ in range(ninj)]
#     injections["f_lower"] = [flow for _ in range(ninj)]
#     injections["amp_order"] = [amporder for _ in range(ninj)]
#     ### FIXME: no numRel Data (hardcoded here)
#     injections["numrel_data"] = ["" for _ in range(ninj)]


#     # # try:
#     # #     injections["mchirp"] = np.array(bound_samples["chirp_mass"])
#     # # except:
#     # #     injections["mchirp"] = np.array(bound_samples["mc"])
#     # # try:
#     # #     injections["eta"] = np.array(bound_samples["symmetric_mass_ratio"])
#     # # except:
#     # #     injections["eta"] = np.array(bound_samples["eta"])
#     # # try:
#     # #     injections["mass1"] = np.array(bound_samples["mass_1"])
#     # # except:
#     # #     injections["mass1"] = np.array(bound_samples["m1"])
#     # # try:
#     # #     injections["mass2"] = np.array(bound_samples["mass_2"])
#     # # except:
#     # #     injections["mass2"] = np.array(bound_samples["m2"])
#     # # injections["geocent_end_time"] = trigtimes_s
#     # # injections["geocent_end_time_ns"] = trigtimes_ns
#     # # try:
#     # #     injections["distance"] = np.array(bound_samples["luminosity_distance"])
#     # # except:
#     # #     injections["distance"] = np.array(bound_samples["dist"])
#     # # injections["longitude"] = np.array(bound_samples["ra"])
#     # # injections["latitude"] = np.array(bound_samples["dec"])
#     # # injections["inclination"] = np.array(bound_samples["iota"])
#     # # injections["coa_phase"] = np.array(bound_samples["phase"])
#     # # injections["polarization"] = np.array(bound_samples["psi"])

#     # # try:
#     # #     injections["spin1x"] = np.array(bound_samples["spin_1x"])
#     # # except:
#     # #     injections["spin1x"] = np.array(bound_samples["a1"]) * np.sin(np.array(bound_samples["theta1"])) * np.cos(
#     # #         np.array(bound_samples["phi1"]))
#     # # try:
#     # #     injections["spin1y"] = np.array(bound_samples["spin_1y"])
#     # # except:
#     # #     injections["spin1y"] = np.array(bound_samples["a1"]) * np.sin(np.array(bound_samples["theta1"])) * np.sin(
#     # #         np.array(bound_samples["phi1"]))
#     # # try:
#     # #     injections["spin1z"] = np.array(bound_samples["spin_1z"])
#     # # except:
#     # #     injections["spin1z"] = np.array(bound_samples["a1"]) * np.cos(np.array(bound_samples["theta1"]))
#     # # try:
#     # #     injections["spin2x"] = np.array(bound_samples["spin_2x"])
#     # # except:
#     # #     injections["spin2x"] = np.array(bound_samples["a2"]) * np.sin(np.array(bound_samples["theta2"])) * np.cos(
#     # #         np.array(bound_samples["phi2"]))
#     # # try:
#     # #     injections["spin2y"] = np.array(bound_samples["spin_2y"])
#     # # except:
#     # #     injections["spin2y"] = np.array(bound_samples["a2"]) * np.sin(np.array(bound_samples["theta2"])) * np.sin(
#     # #         np.array(bound_samples["phi2"]))
#     # # try:
#     # #     injections["spin2z"] = np.array(bound_samples["spin_2z"])
#     # # except:
#     # #     injections["spin2z"] = np.array(bound_samples["a2"]) * np.cos(np.array(bound_samples["theta2"]))

#     # ### take this out: already filled with zeros
#     # ### fill in unused cols with 0's
#     # # for column, dtype in sim_inspiral_dt:
#     # #     if column not in injections.dtype.names:
#     # #         injections[column] = np.zeros(ninj, dtype=dtype)

#     ### write SimInspiralTable from injections array
#     io.SimInspiralIO.writeXML(injections, injfile_name)

