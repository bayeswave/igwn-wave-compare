#!/bin/bash

if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then
  # The script is being sourced in repo directory
  iwc_repo_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)
else
  # The script is being executed directly
  iwc_repo_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")"/.. && pwd)
fi

iwc_pipe_dir=$iwc_repo_dir/iwc_pipe
iwc_hveto_dir=$iwc_pipe_dir/hveto_segs/
iwc_scripts_dir=$iwc_repo_dir/iwc_scripts
iwc_web_utils_dir=$iwc_repo_dir/iwc_web_utils
iwc_package_dir=$iwc_repo_dir/igwn_wave_compare

export IWC_REPO=$iwc_repo_dir

export PATH=$iwc_scripts_dir:$iwc_pipe_dir:$iwc_hveto_dir:$PATH
export PYTHONPATH=$iwc_scripts_dir:$iwc_web_utils_dir:$iwc_package_dir:$iwc_repo_dir:$PYTHONPATH



