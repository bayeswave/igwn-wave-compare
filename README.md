# IGWN Wave Compare
Scripts and libraries for waveform reconstruction and residuals studies.

 ## Quickstart
Preface: `igwn-wave-compare` is designed to be used in the `igwn` environment


- Step 1: clone this repository, and navigate to the appropriate directory:
```
cd igwn-wave-compage
```
- Step 2: `iwc_pipe` uses `bayeswave_pipe`, and so `bayeswave` is required as a submodue. To initialize the submodule, do the following:
```
git submodule init
git submodule update
```
- Step 3: pip install with:
```
pip install .
```
- Step 4: Before running, source the iwc script:
```
source iwc-env.sh
```
