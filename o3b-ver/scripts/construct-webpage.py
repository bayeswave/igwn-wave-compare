#!/cvmfs/oasis.opensciencegrid.org/ligo/sw/conda/envs/igwn-py38-testing/bin/python
# -*- coding: utf-8 -*-
# Copyright (C) 2021  Megan Arogeti <megan.arogeti@ligo.org>
#           (C) 2021 John Michael Sullivan <johnmichael.sullivan@ligo.org>
#           (C) 2021  Sudarshan Ghonge <sudarshan.ghonge@ligo.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

from iwc_web_utils import htmlGen as HTML
import os
import argparse
import pkg_resources

"""
HTML Generation script by Jack Sullivan for LSC resesarch. 

Currently: in testing mode to test HTML output. In process of adding file pathing, tables, and additional info.

(all condiitonal logic commented out)

"""

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument("--onsource-recon-path", default=None, type=str,
                    help="Does this run have onsource reconstructions?")
parser.add_argument("--onsource-res-path", default=None, type=str,
                    help="Does this run have onsource residuals")
# it didnt like offsource-injections in Python
parser.add_argument("--offsource_injections", default=None, type=str,
                    help="Does this run have offsource injections?")
parser.add_argument("--offsource_background", default=None, type=str,
                    help="Does this run have offsource background runs?")
parser.add_argument("--ifos", default=None, nargs='+', choices=['H1', 'L1', 'V1'])

opts = parser.parse_args()

# Generation:
# Copy master.css to working directory

postprocesspath = pkg_resources.resource_filename('iwc_web_utils', '')
os.system('cp ' + postprocesspath + '/master.css ' + '.')

f = open('iwc_output.html', 'w')

bigS = ""

bigS += HTML.head()

# conditional Stuff: 

navDivString = ""

navContentString = ""

# ---------------------------------------
# BEGIN CONDITIONAL LOGIC

# ONSOURCE RECONSTRUCTION SECTION
if opts.onsource_recon_path is not None:
    onsourceReconString = ""
    onsourceReconString += HTML.h1("Onsource Reconstruction", False)
    onsourceReconString += HTML.h2("Reconstruction Comparison", False)
    # alter specifics here

    imagepath = os.path.join(opts.onsource_recon_path, "LI_reconstruct", "reconstruction_comparison.png")
    onsourceReconString += HTML.img(imagepath)

    # table here
    tableAddr = os.path.join(opts.onsource_recon_path, "LI_reconstruct", "all_stats_overlaps.md")
    onsourceReconString += HTML.h2("Overlaps", True) + HTML.table(tableAddr)

    # now package this in div: 
    navDivString += HTML.sectiondiv("Onsource Reconstruction", onsourceReconString)

    navContentString += HTML.navItem("Onsource Reconstruction", True)

# ONSOURCE RESIDUALS SECTION
if opts.onsource_res_path is not None:
    onsourceResString = ""
    onsourceResString += HTML.h1("Onsource Residuals", False)
    onsourceResString += HTML.h2("Whitened Data", False)
    # something here! 

    imagepath = os.path.join(os.path.dirname(opts.onsource_res_path), "residual_frame", "white_data_res.png")
    onsourceResString += HTML.img(imagepath)

    onsourceResString += HTML.h2("Signal Waveform", False)

    # imagepath = glob.glob('test_assets/snr_residuals.png')[0]
    imagepaths = [os.path.join(opts.onsource_res_path, "plots", "signal_waveform_{}.png".format(ifo))
                  for ifo in opts.ifos]

    onsourceResString += HTML.imgGridVar(imagepaths)

    # TESTING NEW GRID COMPONENT; COMMENTING BELOW OUT
    # for imagepath in imagepaths:
    #     onsourceResString += HTML.img(imagepath)

    # Combining:
    navDivString += HTML.sectiondiv("Onsource Residuals", onsourceResString)
    navContentString += HTML.navItem("Onsource Residuals", False)  # not the default active in the menu anymore

# OFFSOURCE INJECTIONS SECTION
if opts.offsource_injections is not None:
    offsourceInjString = ""
    offsourceInjString += HTML.h1("Offsource Injections", False)
    offsourceInjString += HTML.h2("Overlap Histogram", False)

    imagepath = "offsource_injections/overlap_files/catalog_comparison.png"
    offsourceInjString += HTML.img(imagepath)

    # store button location in variable here! ->
    buttonAddress = "offsource_injections/overlap_files/overlaps.dat"
    offsourceInjString += HTML.linkButton(buttonAddress)

    # Combine for page structure!
    navDivString += HTML.sectiondiv("Offsource Injections", offsourceInjString)
    navContentString += HTML.navItem("Offsource Injections", False)

# OFFSOURCE BACKGROUND SECTION
if opts.offsource_background is not None:
    offsourceBckgrndString = ""
    offsourceBckgrndString += HTML.h1("Offsource Background", False)

    offsourceBckgrndString += HTML.h2("SNR 90 Background", False)

    imagepath = "offsource_background/moments_files/snr_residuals.png"
    offsourceBckgrndString += HTML.img(imagepath)

    # store download button location in variable here! ->
    buttonAddress = "offsource_background/moments_files/offsource_snr_90s.dat"
    offsourceBckgrndString += HTML.linkButton(buttonAddress)

    # Combine!!!
    navDivString += HTML.sectiondiv("Offsource Background", offsourceBckgrndString)
    navContentString += HTML.navItem("Offsource Background", False)

# IF 1 & 3
if opts.onsource_recon_path is not None and opts.offsource_injections is not None:
    reconAnalysisStr = ""
    reconAnalysisStr += HTML.h1("Reconstruction Analysis", False)
    reconAnalysisStr += HTML.h2("Catalog Comparison", False)
    imagepath = "catalog/catalog_output/catalog_comparison.png"
    reconAnalysisStr += HTML.img(imagepath)
    navDivString += HTML.sectiondiv("Reconstruction Analysis", reconAnalysisStr)
    navContentString += HTML.navItem("Reconstruction Analysis", False)
# IF 2 & 4

if opts.onsource_res_path is not None and opts.offsource_background is not None:
    resAnalysisStr = ""
    resAnalysisStr += HTML.h1("Residual Analysis", False)
    resAnalysisStr += HTML.h2("SNR Residuals", False)
    imagepath = "testgr/testgr_output/snr_residuals.png"
    resAnalysisStr += HTML.img(imagepath)

    navDivString += HTML.sectiondiv("Residual Analysis", resAnalysisStr)
    navContentString += HTML.navItem("Residual Analysis", False)

# HTML.navBones(navContentString)
bigS += (HTML.navBones(navContentString)) + HTML.container("bodContent", navDivString)
bigS += HTML.footer()

f.write(bigS)
f.close()
